import React from 'react';

import Header from './Generic/Header';

export default function Home() {
  return (
    <div className="home">
      <Header headerInfo="Witaj Jakub" />
      Home
    </div>
  );
}
