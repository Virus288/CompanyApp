import React from 'react';
import { Link } from 'react-router-dom';

import '../../Css/fontello/css/fontello.css';

export default function Navbar() {
  const enableNav = () => {
    document.querySelector('.navbar')?.classList.toggle('navbar-inactive');
    document.querySelector('.navbar')?.classList.toggle('navbar-active');

    document
      .querySelector('.navbarToggle')
      ?.classList.toggle('navbarToggle-inactive');
    document
      .querySelector('.navbarToggle')
      ?.classList.toggle('navbarToggle-active');
  };

  const toggleIcon = (target: HTMLElement) => {
    document
      .querySelector('.navIcon-active')
      ?.classList.toggle('navIcon-active');
    target.classList.toggle('navIcon-active');
  };

  return (
    <>
      <div className="navbar navbar-inactive">
        <span id="home" className="navIcon navIcon-inactive">
          <Link
            to="/"
            style={{ textDecoration: 'none' }}
            replace
            onClick={(e) => {
              const target = e.target as Element;
              toggleIcon(target.parentElement!);
            }}
          >
            <img src="../assets/icons/navIcons/home.png" alt="home" />
          </Link>
        </span>

        <span id="employees" className="navIcon navIcon-inactive">
          <Link
            to={{ pathname: '/employees', state: { component: 'Main' } }}
            style={{ textDecoration: 'none' }}
            replace
            onClick={(e) => {
              const target = e.target as Element;
              toggleIcon(target.parentElement!);
            }}
          >
            <img src="../assets/icons/navIcons/home.png" alt="home" />
          </Link>
        </span>

        <span id="stores" className="navIcon navIcon-inactive">
          <Link
            to="/stores"
            style={{ textDecoration: 'none' }}
            replace
            onClick={(e) => {
              const target = e.target as Element;
              toggleIcon(target.parentElement!);
            }}
          >
            <img src="../assets/icons/navIcons/home.png" alt="home" />
          </Link>
        </span>

        <span id="payments" className="navIcon navIcon-inactive">
          <Link
            to="/payments"
            style={{ textDecoration: 'none' }}
            replace
            onClick={(e) => {
              const target = e.target as Element;
              toggleIcon(target.parentElement!);
            }}
          >
            <img src="../assets/icons/navIcons/home.png" alt="home" />
          </Link>
        </span>

        <span id="invoices" className="navIcon navIcon-inactive">
          <Link
            to="/invoices"
            style={{ textDecoration: 'none' }}
            replace
            onClick={(e) => {
              const target = e.target as Element;
              toggleIcon(target.parentElement!);
            }}
          >
            <img src="../assets/icons/navIcons/home.png" alt="home" />
          </Link>
        </span>
      </div>
      <img
        className="navbarToggle navbarToggle-inactive"
        onClick={() => enableNav()}
        src="../assets/svg/arrow.svg"
        alt="navToggler"
      />
    </>
  );
}
