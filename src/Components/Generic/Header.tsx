import React from 'react';
import { Link } from 'react-router-dom';

interface Data {
  headerInfo: string;
}

export default function Header(data: Data) {
  return (
    <div className="header">
      <span className="headerInfo">
        <h2>{data.headerInfo}</h2>
      </span>
      <span className="headerIcons">
        <img src="../assets/icons/navIcons/home.png" alt="home" />
        <Link to="/messages" style={{ textDecoration: 'none' }} replace>
          <img src="../assets/icons/navIcons/home.png" alt="home" />
        </Link>
        <Link to="/account" style={{ textDecoration: 'none' }} replace>
          <img src="../assets/icons/navIcons/home.png" alt="home" />
        </Link>
      </span>
    </div>
  );
}
