import React, { useEffect } from 'react';
import { remote } from 'electron';
import { Link } from 'react-router-dom';
import Header from '../Generic/Header';

const fs = require('fs');

export default function CreateInvoice() {
  useEffect(() => {
    fs.readdir(
      `${remote.app.getPath('userData')}/invoices`,
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (err: any, data: any) => {
        console.log(data);
    });
  });

  return (
    <div className="invoices">
      <Header headerInfo="Faktury" />

      <span className="upperInvoices">
        <h2>Poprzednie faktury</h2>
        <Link
          to={{ pathname: '/addinvoice' }}
          style={{ textDecoration: 'none' }}
          replace
        >
          <h4 className="addInvoice">+ Dodaj fakturę</h4>
        </Link>
      </span>
      <div className="recentInvoices">
        <div className="invoice">
          <img src="../assets/icons/navIcons/home.png" alt="home" />
          <h4>Opłata za prąd 2020.02.21</h4>
        </div>
        <div className="invoice">
          <img src="../assets/icons/navIcons/home.png" alt="home" />
          <h4>Opłata za prąd 2020.02.21</h4>
        </div>
        <div className="invoice">
          <img src="../assets/icons/navIcons/home.png" alt="home" />
          <h4>Opłata za prąd 2020.02.21</h4>
        </div>
      </div>
    </div>
  );
}
