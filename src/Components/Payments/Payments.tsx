import React from 'react';
import Header from '../Generic/Header';

export default function Payments() {
  return (
    <div className="payments">
      <Header headerInfo="Opłaty" />
      Payments
    </div>
  );
}

// Widok główny:
// - Informacje, o tym, ile wynosi opłata za sklep, po wejściu w dany rodzaj opłaty jak sklep, informacje o przykładowo "woda: xxx, prąd:xxx, pracownik: xxx"
// - Faktury: wystawianie, zdjęcia z poprzednich ( ikonki dość małe ale widoczne ), styl graficzny na bazie karty z boootstrapa, na górze zdjęcie, niżej informacja o kwocie
// - Dodawanie, kasowanie opłat, faktur etc etc
