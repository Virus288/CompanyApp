import React, { useState } from 'react';
import Header from '../Generic/Header';

import RenderInvoice from './RenderInvoice';

export default function AddInvoice() {
  const [display, setDisplay] = useState(false);

  return (
    <div>
      <Header headerInfo="Dodawanie faktury" />
      Add Invoice
      <button type="button" onClick={() => setDisplay(true)}>Create invoice</button>
      <RenderInvoice {...{ display, setDisplay }} />
    </div>
  );
}
