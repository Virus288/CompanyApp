import React, { useEffect } from 'react';

import { AnimatePresence, motion } from 'framer-motion';
import { CreatePdf } from './CreatePdf';
import { slideBottom } from '../../Animation/Variables';

interface data {
  display: boolean;
  setDisplay: () => void;
}

export default function RenderInvoice(data: data) {
  const Pdf = new CreatePdf('Capri');

  useEffect(() => {
    if (data.display) {
      Pdf.Submit();
    }
  });

  return (
    <AnimatePresence exitBeforeEnter>
      {data.display ? (
        <motion.div
          className="invoiceRendered"
          variants={slideBottom}
          initial="init"
          animate="visible"
          exit="exit"
        >
          <iframe className="invoiceFrame" title="Invoice" id="pdf" />
          <h2
            className="exit"
            role="presentation"
            onClick={() => {
              data.setDisplay();
            }}
          >
            X
          </h2>
        </motion.div>
      ) : (
        ''
      )}
    </AnimatePresence>
  );
}
