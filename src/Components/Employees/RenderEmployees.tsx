import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

// Database
import { EmployeesDB } from '../../Db';

// Internal modules
import { disablePopup, enablePopup } from '../../Redux/actions';

interface EmployeesData {
  display: boolean;
  employeeChanged: boolean;
  setEmployee: (data: string) => void;
  setEmployeeChanged: (data: boolean) => void;
  setDisplay: () => void;
}

interface Employee {
  firstName: string;
  secondName: string;
  age: string;
  workPlace: string;
  salary: string;
  role: string;
  status: string;
}

const RenderEmployees: React.FC<EmployeesData> = (data) => {
  const dispatch = useDispatch();

  const [employees, setEmployees] = React.useState<Employee[]>([]);
  const [sorted, setSorted] = useState(Boolean);
  const [filteredEmployees, setFilteredEmployees] = React.useState<Employee[]>(
    []
  );
  const [enabled, setEnabled] = useState(Array);

  useEffect(() => {
    EmployeesDB.loadDatabase();
    EmployeesDB.find({}, (err: string, results: Array<Employee>) => {
      if (err) {
        console.log(err);
        dispatch(enablePopup('Wystąpił błąd'));
        setTimeout(() => {
          dispatch(disablePopup());
        }, 3000);
      }
      setEmployees(results);
      setFilteredEmployees(results);
    });
  }, [dispatch, data.employeeChanged]);

  useEffect(() => {}, [sorted]);

  const removeEmployee = (employee: string) => {
    try {
      EmployeesDB.remove({ _id: employee }, (err: never) => {
        if (err) {
          dispatch(enablePopup('Wystąpił błąd'));
          setTimeout(() => {
            dispatch(disablePopup());
          }, 3000);
          console.log(err);
        } else {
          dispatch(enablePopup('Suckes'));
          setTimeout(() => {
            dispatch(disablePopup());
          }, 3000);
        }
        return data.setEmployeeChanged(true);
      });
    } catch (err) {
      console.error(err);
    }
  };

  const toogleTR = (itemID: string) => {
    const toggleALL = document.querySelector(
      '.tableInputAll'
    ) as HTMLInputElement;

    if (enabled.includes(itemID)) {
      let innerData = enabled;
      innerData = innerData.filter((item) => {
        return item !== itemID;
      });
      setEnabled(innerData);
      document
        .getElementById(`${itemID}`)
        ?.classList.toggle('employeeTR-active');
      document
        .getElementById(`${itemID}`)
        ?.classList.toggle('employeeTR-inactive');

      toggleALL.checked = false;
    } else {
      const innerData = enabled;
      innerData.push(itemID);
      setEnabled(innerData);
      document
        .getElementById(`${itemID}`)
        ?.classList.toggle('employeeTR-inactive');
      document
        .getElementById(`${itemID}`)
        ?.classList.toggle('employeeTR-active');

      const allInactive = document.querySelectorAll(`.employeeTR-inactive`);
      if (allInactive.length === 0) {
        toggleALL.checked = true;
      }
    }
  };

  const sort = (type: string) => {
    const newEmployees = filteredEmployees.sort((a, b) => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      if (a[type] < b[type]) return -1;
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      if (a[type] > b[type]) return 1;
      return 0;
    });
    setFilteredEmployees(newEmployees);
    setSorted(!sorted);
  };

  const preSort = () => {
    const filterValue = document.querySelector('.filter') as HTMLInputElement;
    switch (filterValue.value) {
      case 'Id':
        sort('_id');
        break;
      case 'Imię':
        sort('firstName');
        break;
      case 'Nazwisko':
        sort('secondName');
        break;
      case 'Rola':
        sort('role');
        break;
      case 'Miejsce':
        sort('workPlace');
        break;
      case 'Status':
        sort('status');
        break;
      default:
        sort('status');
        break;
    }
  };

  const toggleAll = (item: HTMLInputElement) => {
    let localData = enabled;
    const allInactive = document.querySelectorAll(`.employeeTR-inactive`);
    const allActive = document.querySelectorAll(`.employeeTR-active`);

    if (item.checked) {
      allInactive.forEach((element) => {
        const checkbox = element.children[0].children[0] as HTMLInputElement;

        element.classList.toggle('employeeTR-inactive');
        element.classList.toggle('employeeTR-active');
        checkbox.checked = true;

        localData.push(element.id);
      });
    } else {
      allActive.forEach((element) => {
        const checkbox = element.children[0].children[0] as HTMLInputElement;

        element.classList.toggle('employeeTR-inactive');
        element.classList.toggle('employeeTR-active');
        checkbox.checked = false;
      });
      localData = [];
    }

    setEnabled(localData);
  };

  const noEmployees = () => {
    return <span>Wygląda na to, iż nie dodałeś żadnych pracowników.</span>;
  };

  const renderEmployees = () => {
    return filteredEmployees.map((employee) => {
      return (
        <tr
          id={Object(employee)._id}
          key={Object(employee)._id}
          className="employeeTR-inactive"
        >
          <td className="tableInputTd">
            <input
              type="checkbox"
              className="tableInput"
              onChange={(e) => {
                const target = e.target as Element;
                toogleTR(target.parentElement?.parentElement?.id ?? '');
              }}
            />
          </td>
          <td>
            <img
              src="../assets/img/user.png"
              alt="profilePic"
              className="profilePic"
            />
          </td>
          <td>
            {Object(employee).firstName} {Object(employee).secondName}
          </td>
          <td>{Object(employee).role}</td>
          <td>{Object(employee).workPlace}</td>
          <td>{Object(employee).status}</td>
          <td>
            <img src="../assets/img/dots.png" alt="dots" />
          </td>
        </tr>
      );
    });
  };

  return (
    <div className="employeesRender">
      <div className="employeesTop">
        <span>
          <input
            type="text"
            placeholder="Szukaj"
            className="findEmployees"
            onChange={(e) => {
              if (e.target.value.length === 0) {
                setFilteredEmployees(employees);
              } else {
                const newEmployees: Array<Employee> = [];

                for (let x = 0; x < employees.length; x++) {
                  if (
                    employees[x].firstName
                      .toLowerCase()
                      .includes(e.target.value.toLowerCase()) ||
                    employees[x].secondName
                      .toLowerCase()
                      .includes(e.target.value.toLowerCase())
                  ) {
                    newEmployees.push(employees[x]);
                  }
                }
                setFilteredEmployees(newEmployees);
              }
            }}
          />
          <Link
            to={{ pathname: '/addemployee', state: { component: 'Main' } }}
            style={{ textDecoration: 'none' }}
            replace
          >
            <h4 className="addEmployee">+ Dodaj pracownika</h4>
          </Link>
        </span>

        <span>
          <span>
            <span>
              <input
                type="checkbox"
                className="tableInputAll"
                style={{
                  height: '30px',
                  marginBottom: '7px',
                  marginRight: '5px',
                }}
                onChange={(e) => {
                  const target = e.target as HTMLInputElement;
                  toggleAll(target);
                }}
              />
              <h4
                role="presentation"
                onClick={() => {
                  const target = document.querySelector(
                    '.tableInputAll'
                  ) as HTMLInputElement;
                  target.checked = !target.checked;
                  toggleAll(target);
                }}
              >
                Wybierz wszystkich
              </h4>
            </span>
            <h4
              role="presentation"
              onClick={() => {
                const allActive = document.querySelectorAll(
                  `.employeeTR-active`
                );
                if (allActive.length > 1 || allActive.length === 0) {
                  dispatch(
                    enablePopup('Wybierz 1 osobę, którą chcesz edytować')
                  );
                  setTimeout(() => {
                    dispatch(disablePopup());
                  }, 3000);
                } else {
                  data.setDisplay();
                  data.setEmployee(allActive[0].id);

                  allActive.forEach((element) => {
                    const checkbox = element.children[0]
                      .children[0] as HTMLInputElement;

                    element.classList.toggle('employeeTR-inactive');
                    element.classList.toggle('employeeTR-active');
                    checkbox.checked = false;
                  });
                }
              }}
            >
              Edytuj
            </h4>
            <h4
              role="presentation"
              onClick={() => {
                const allActive = document.querySelectorAll(
                  `.employeeTR-active`
                );
                const ids: string[] = [];

                allActive.forEach((item) => {
                  ids.push(item.id);
                });
                ids.forEach((id) => {
                  removeEmployee(id);
                });
              }}
            >
              Usuń
            </h4>
          </span>

          <span>
            <h4>Sortuj</h4>
            <select className="filter" name="filter" onChange={() => preSort()}>
              <option value="Id" defaultValue="Id">
                Id
              </option>
              <option value="Imię">Imię</option>
              <option value="Nazwisko">Nazwisko</option>
              <option value="Rola">Rola</option>
              <option value="Miejsce">Miejsce pracy</option>
              <option value="Status">Status</option>
            </select>
          </span>
        </span>
      </div>
      <table className="employeeseTable">
        <tbody>
          <tr>
            <td style={{ maxWidth: '20px' }}> </td>
            <td> </td>
            <td>Pracownik: </td>
            <td>Rola w firmie:</td>
            <td>Pracuje w:</td>
            <td>Status:</td>
            <td style={{ maxWidth: '20px' }} />
          </tr>
          {employees.length > 0 ? (
            renderEmployees()
          ) : (
            <tr>
              <td style={{ display: 'none' }}> </td>
            </tr>
          )}
        </tbody>
      </table>
      {employees.length > 0 ? '' : noEmployees()}
    </div>
  );
};

export default RenderEmployees;
