import React, { useState, useEffect } from 'react';

// Internal modules
import Header from '../Generic/Header';
import RenderEmployees from './RenderEmployees';
import EditEmployee from './EditEmployee';

export default function Employees() {
  const [display, setDisplay] = useState(false);
  const [employee, setEmployee] = useState(String);
  const [employeeChanged, setEmployeeChanged] = useState<boolean>(false);

  useEffect(() => {
    document
      .querySelector('.navIcon-active')
      ?.classList.toggle('navIcon-active');

    document.querySelector('#employees')?.classList.toggle('navIcon-active');
  }, []);

  return (
    <div className="employeesMain">
      <Header headerInfo="Pracownicy" />
      <RenderEmployees
        {...{
          display,
          setEmployee,
          employeeChanged,
          setEmployeeChanged,
          setDisplay: () => setDisplay(true),
        }}
      />
      <EditEmployee
        {...{
          display,
          employee,
          setEmployeeChanged,
          setDisplay: () => setDisplay(false),
        }}
      />
    </div>
  );
}
