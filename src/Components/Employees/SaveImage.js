import { remote } from 'electron';

const fs = require('fs');

fs.writeFile(
  `${remote.app.getPath('userData')}/banana.txt`,
  'UserStorage Test',
  (err) => {
    if (err) {
      return console.log(err);
    }
    return console.log('The file was saved!');
  }
);
