import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router';

// Database
import { EmployeesDB } from '../../Db';

// Redux
import { enablePopup, disablePopup } from '../../Redux/actions';

EmployeesDB.loadDatabase();

const AddEmployee = (props: RouteComponentProps) => {
  const [state, setstate] = useState(Object);

  const dispatch = useDispatch();

  console.log(props);

  const addNewEmployee = () => {
    const form = document.querySelector('form') as HTMLFormElement;

    const data = {
      firstName: form.firstName.value,
      secondName: form.secondName.value,
      age: form.age.value,
      workPlace: form.workPlace.value,
      salary: form.salary.value,
      role: form.role.value,
      status: form.status.value,
    };

    EmployeesDB.insert(data, (err: string) => {
      if (err) {
        dispatch(enablePopup('Wystąpił błąd'));
        setTimeout(() => {
          dispatch(disablePopup());
        }, 3000);
        return setstate({ type: 'error', message: err });
      }
      dispatch(enablePopup('Pracownik został dodany'));
      setTimeout(() => {
        dispatch(disablePopup());
      }, 3000);

      return props.history.push({
        pathname: '/employees',
        state: { registered: true },
      });
    });
  };

  const renderForm = () => {
    return (
      <form
        className="addEmployeePannel"
        onSubmit={(e) => {
          e.preventDefault();
          addNewEmployee();
        }}
      >
        <label htmlFor="firstName">
          <h2>Imię</h2>
          <input type="text" name="firstName" placeholder="Imię" required />
        </label>

        <label htmlFor="secondName">
          <h2>Nazwisko</h2>
          <input
            type="text"
            name="secondName"
            placeholder="Nazwisko"
            required
          />
        </label>

        <label htmlFor="age">
          <h2>Wiek</h2>
          <input type="date" name="age" placeholder="Wiek" required />
        </label>

        <label htmlFor="workPlace">
          <h2>Miejsce pracy</h2>
          <input type="text" name="workPlace" placeholder="" />
        </label>

        <label htmlFor="role">
          <h2>Rola w firmie</h2>
          <input type="text" name="role" placeholder="Rola w firmie" required />
        </label>

        <label htmlFor="salary">
          <h2>Wypłata</h2>
          <input type="number" name="salary" placeholder="Wypłata" required />
        </label>

        <label htmlFor="status">
          <h2>Rodzaj zatrudnienia</h2>
          <select name="status" defaultChecked required>
            <option value="Pełny etat" defaultValue="Pełny etat">
              Pełny etat
            </option>
            <option value="Pół etatu">Pół etatu</option>
            <option value="Zdalnie">Zdalnie</option>
          </select>
        </label>

        <button type="submit">Dodaj</button>
      </form>
    );
  };

  return (
    <div className="employeeForm">
      {state.type === 'success' ? 'Dodano pracownika' : renderForm()}
    </div>
  );
};

export default withRouter(AddEmployee);
