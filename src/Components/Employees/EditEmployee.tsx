import React, { useEffect, useState } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { useDispatch } from 'react-redux';

// Animation
import { slideBottom } from '../../Animation/Variables';

// Database
import { EmployeesDB } from '../../Db';

// Internal modules
import { enablePopup, disablePopup } from '../../Redux/actions';

interface Data {
  employee: string;
  display: boolean;
  setEmployeeChanged: (data: boolean) => void;
  setDisplay: () => void;
}

interface Employee {
  firstName: string;
  secondName: string;
  age: string;
  workPlace: string;
  salary: string;
  role: string;
  status: string;
}

const EditEmployee = (data: Data) => {
  const [employee, setEmployee] = useState<Employee>();
  const dispatch = useDispatch();

  useEffect(() => {
    EmployeesDB.loadDatabase();
    EmployeesDB.find({ _id: data.employee }, (err: string, results: never) => {
      if (err) {
        console.log(err);
        dispatch(enablePopup('Wystąpił błąd'));
        setTimeout(() => {
          dispatch(disablePopup());
        }, 3000);
      }
      setEmployee(results[0]);
    });
  }, [data, dispatch]);

  const update = () => {
    const form = document.querySelector('form') as HTMLFormElement;

    const newEmployee = {
      firstName: form.firstName.value,
      secondName: form.secondName.value,
      age: form.age.value,
      workPlace: form.workPlace.value,
      role: form.role.value,
      salary: form.salary.value,
      status: form.status.value,
    };

    EmployeesDB.update(
      { _id: data.employee },
      { ...newEmployee },
      { multi: true },
      (err: unknown) => {
        if (err) {
          dispatch(enablePopup('Wystąpił błąd'));
          setTimeout(() => {
            dispatch(disablePopup());
          }, 3000);
        }
        data.setDisplay();
        data.setEmployeeChanged(true)
        dispatch(enablePopup('Dane zostały zmienione poprawnie'));
        setTimeout(() => {
          dispatch(disablePopup());
        }, 3000);
      }
    );
  };

  if (employee) {
    const form = document.querySelector('form') as HTMLFormElement;
    if (form !== null) {
      form.firstName.value = employee.firstName;
      form.secondName.value = employee.secondName;
      form.age.value = employee.age;
      form.workPlace.value = employee.workPlace;
      form.role.value = employee.role;
      form.salary.value = employee.salary;
      form.status.value = employee.status;
    }
  }

  return (
    <AnimatePresence exitBeforeEnter>
      {data.display ? (
        <motion.div
          className="editEmployee"
          variants={slideBottom}
          initial="init"
          animate="visible"
          exit="exit"
        >
          <form
            className="editEmployeeForm"
            onSubmit={(e) => {
              e.preventDefault();
              try {
                return update();
              } catch (error) {
                console.log(error);
                return ' ';
              }
            }}
          >
            <label htmlFor="firstName">
              <span>Imię</span>
              <input type="text" name="firstName" placeholder="Imię" required />
            </label>

            <label htmlFor="secondName">
              <span>Nazwisko</span>
              <input
                type="text"
                name="secondName"
                placeholder="Nazwisko"
                required
              />
            </label>

            <label htmlFor="age">
              <span>Wiek</span>
              <input type="date" name="age" placeholder="Wiek" required />
            </label>

            <label htmlFor="workPlace">
              <span>Miejsce pracy</span>
              <input type="text" name="workPlace" placeholder="Miejsce pracy" />
            </label>

            <label htmlFor="role">
              <span>Rola w firmie</span>
              <input
                type="text"
                name="role"
                placeholder="Rola w firmie"
                required
              />
            </label>

            <label htmlFor="salary">
              <span>Wypłata</span>
              <input
                type="number"
                name="salary"
                placeholder="Wypłata"
                required
              />
            </label>

            <label htmlFor="status">
              <span>Rodzaj zatrudnienia</span>
              <select name="status" defaultChecked required>
                <option value="Pełny etat" defaultValue="Pełny etat">
                  Pełny etat
                </option>
                <option value="Pół etatu">Pół etatu</option>
                <option value="Zdalnie">Zdalnie</option>
              </select>
            </label>

            <button type="submit">Zatwierdź</button>
            <h2
              className="exit"
              role="presentation"
              onClick={() => {
                data.setDisplay();
              }}
            >
              X
            </h2>
          </form>
        </motion.div>
      ) : (
        ''
      )}
    </AnimatePresence>
  );
};

export default EditEmployee;
