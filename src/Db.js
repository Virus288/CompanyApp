const Datastore = require('nedb');

// Stores
const StoresDB = new Datastore('stores.db');

// Employees
const EmployeesDB = new Datastore('employees.db');

// Invoice
const InvoicesDB = new Datastore('invoice.db');

module.exports = {
  StoresDB,
  EmployeesDB,
  InvoicesDB,
};

// import {Database} from "../Db"
// Database.loadDatabase()

// Insert
// Database.insert({name: "japko", age: 15});

// Find
// Database.find({_id: "xk0om8jNoZjVtmpp"}, (err, data) => {
//   console.log(data)
// })

// Remove
// Database.remove({_id: "xk0om8jNoZjVtmpp"}, (err, data) => {
//   console.log(data)
// })
