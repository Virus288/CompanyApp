import React from 'react';
import { HashRouter as Router } from 'react-router-dom';
import './Css/style.global.css';

// Internal modules
import Navbar from './Components/Generic/Navbar';
import Popup from './Components/Generic/Popup';

// Routers
import MainRouter from './Routers/Router';
import EmployeesRouter from './Routers/Router_Employees';
import PaymentsRouter from './Routers/Router_Payments';
import StoresRouter from './Routers/Router_Stores';
import AccountsRouter from './Routers/Router_Accounts';

export default function App() {
  return (
    <Router>
      <div className="App app-inactive">
        <Navbar />
        <Popup />
        <MainRouter />
        <EmployeesRouter />
        <PaymentsRouter />
        <StoresRouter />
        <AccountsRouter/>
      </div>
    </Router>
  );
}
