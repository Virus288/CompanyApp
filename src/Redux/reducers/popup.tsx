interface action {
  type: string,
    payload: {
    toggle: boolean,
    data: string
  }
}

const popupActivator = (state = {toggle: false, data: ""}, action: action) => {
    switch(action.type) {
        case "ENABLE":
            return state = {toggle: true, data: action.payload.data}
        case "DISABLE":
            return state = {toggle: false, data: ""}
        default:
            return state
    }
}
export default popupActivator;
