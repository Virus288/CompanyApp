import { combineReducers } from 'redux';
import popupActivator from './popup';

const rootReducer = combineReducers({
  popup: popupActivator,
});

export default rootReducer;
