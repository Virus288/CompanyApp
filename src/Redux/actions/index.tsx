export const enablePopup = (data) => {
    return {
        type: "ENABLE",
        payload: {data: data}
    }
}

export const disablePopup = () => {
    return {
        type: "DISABLE"
    }
}
