// Imports
import React from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';

// Routes
// Stores general
import Stores from '../Components/Stores/Stores';

function StoresRouter() {
  const location = useLocation()

  return (
    <Switch location={location} key={location.key}>
      <Route exact path="/stores" render={() => (
        <Stores/>
      )}/>
    </Switch>
  )
}

export default StoresRouter;
