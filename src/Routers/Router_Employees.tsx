// Imports
import React from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';

// Routes
// Employees main
import Employees from '../Components/Employees/Employees';
// Add employees
import AddEmployee from '../Components/Employees/AddEmployee';

function EmployeesRouter() {
  const location = useLocation();

  return (
    <Switch location={location} key={location.key}>
      <Route exact path="/employees" render={() => <Employees />} />
      <Route exact path="/addemployee" render={() => <AddEmployee />} />
    </Switch>
  );
}

export default EmployeesRouter;
