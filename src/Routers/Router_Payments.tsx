// Imports
import React from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';

// Routes
// Payments general
import Payments from '../Components/Payments/Payments';

// Invoices general
import Invoices from '../Components/Payments/Invoices';

// Add payments
import AddPayment from '../Components/Payments/AddPayment';

// Add invoice
import AddInvoice from '../Components/Payments/AddInvoice';

function PaymentsRouter() {
  const location = useLocation();

  return (
    <Switch location={location} key={location.key}>
      <Route exact path="/payments" render={() => <Payments />} />
      <Route exact path="/invoices" render={() => <Invoices />} />
      <Route exact path="/addinvoice" render={() => <AddInvoice />} />
      <Route exact path="/addpayment" render={() => <AddPayment />} />
    </Switch>
  );
}

export default PaymentsRouter;
