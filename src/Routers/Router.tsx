// Imports
import React from 'react';
import { Switch, Route, useLocation } from 'react-router-dom';

// Home
import Home from '../Components/Home';

function MainRouter() {
  const location = useLocation();

  return (
    <Switch location={location} key={location.key}>
      <Route exact path="/" render={() => <Home />} />
    </Switch>
  );
}

export default MainRouter;
