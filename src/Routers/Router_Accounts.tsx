// Imports
import React from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';

// Routes
// Main account
import Account from '../Components/Account/Account';
import Messages from '../Components/Account/Messages';

function AccountsRouter() {
  const location = useLocation();

  return (
    <Switch location={location} key={location.key}>
      <Route exact path="/account" render={() => <Account />} />
      <Route exact path="/messages" render={() => <Messages />} />
    </Switch>
  );
}

export default AccountsRouter;
